/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.sim.core

import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import straightway.sim.Controller
import straightway.sim.Scheduler
import straightway.utils.TimeProvider

@Suppress("USELESS_IS_CHECK")
internal class SimulatorTestInterfaces : SimulatorTest() {
    @Test
    fun isTimeProvider() = assertTrue(sut is TimeProvider)

    @Test
    fun isSimulationController() = assertTrue(sut is Controller)

    @Test
    fun isSimulationScheduler() = assertTrue(sut is Scheduler)
}